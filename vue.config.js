module.exports = {
  devServer: {
    proxy: process.env.API_URL || 'http://localhost:3000'
  }
}