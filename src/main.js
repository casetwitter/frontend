import Vue from 'vue'
import App from './App.vue'
import Exerc1 from './Exerc1.vue'
import Usuario from './Usuario.vue'
import VueRouter from 'vue-router';
import 'bootstrap/dist/css/bootstrap.min.css'

export const routes = [
  { path: '/', component: Exerc1 },
  { path: '/users', component: Usuario }
];

Vue.use(VueRouter);

const router = new VueRouter({
  routes,
  mode: 'history'
});

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
