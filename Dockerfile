FROM node:lts-alpine

RUN npm install -g http-server

ENV URL_API ${URL_API}

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build

EXPOSE 8080

CMD [ "http-server", "." ]