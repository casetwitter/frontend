# Front-end

Repositório com código fonte do front-end da aplicação.

**Importante**

Todos os comandos aqui apresentados devem ser executados na raiz deste projeto.

## Pré-requisitos

Antes da execução da aplicação é preciso verificar se estão instalados todos os recursos necessários para a execução do front-end. Abaixo segue a lista com os recursos.

- Plataforma NodeJS
- Gerenciador de pacotes NPM
- Plataforma de container Docker

## Instalação

Primeiro faço *download* de todas as dependências do projeto.

```bash
npm i
```

## Execução

Utilize o comando abaixo para executar a aplicação.

```bash
npm run dev
```

## Gerar binários para Produção

Para gerar os binários para o ambiente de produção execute o comando:

```bash
npm run build
```

## Geração da imagem Docker

Caso seja necessário gerar a imagem Docker da aplicação execute o seguinte comando:

```bash
docker build -t frontend_casetwitter .
```

## Execução em docker-compose

Para executar todo o projeto em `docker-compose` acesse [este projeto]().