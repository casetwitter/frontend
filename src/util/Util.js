export default class Util {

  constructor() {
    throw new Error('Esta classe não pode ser instanciada');
  }

  static getURLAPI() {
    return (process.env.URL_API) ? process.env.URL_API : 'http://localhost:3000';
  }

  static getTags() {
    return tags = [
      "#cloud",
      "#container",
      "#devops",
      "#aws",
      "#microservices",
      "#docker",
      "#openstack",
      "#automation",
      "#gcp",
      "#azure",
      "#istio",
      "#sre"
    ];
  }
}
