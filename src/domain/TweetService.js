import axios from 'axios';
import Util from "../util/Util";

export default class TweetService {
  TweetsByHashtag(hashtag) {
    return axios.get(`${Util.getURLAPI()}/api/tweet/`, {
      params: {
        hashtag
      }
    });
  }

  usersByHashtag() {
    return axios.get(`${Util.getURLAPI()}/api/tweet/users`);
  }

  hashtagsLoaded() {
    return axios.get(`${Util.getURLAPI()}/api/tweet/hashtag/stored`);
  }
}
